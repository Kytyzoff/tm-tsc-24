package ru.tsc.borisyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.command.AbstractTaskCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.entity.TaskNotFoundException;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findByName(userId, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

package ru.tsc.borisyuk.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.borisyuk.tm.command.AbstractCommand;

public class CommandsShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of commands...";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            String commandName = command.name();
            if (commandName != null && !commandName.isEmpty())
                System.out.println(commandName + ": " + command.description());
        }
    }

}

package ru.tsc.borisyuk.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.borisyuk.tm.api.repository.ICommandRepository;
import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.repository.IUserRepository;
import ru.tsc.borisyuk.tm.api.service.*;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.command.user.*;
import ru.tsc.borisyuk.tm.constant.TerminalConst;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.exception.system.UnknownCommandException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.repository.CommandRepository;
import ru.tsc.borisyuk.tm.repository.ProjectRepository;
import ru.tsc.borisyuk.tm.repository.TaskRepository;
import ru.tsc.borisyuk.tm.repository.UserRepository;
import ru.tsc.borisyuk.tm.service.*;
import ru.tsc.borisyuk.tm.command.task.*;
import ru.tsc.borisyuk.tm.command.project.*;
import ru.tsc.borisyuk.tm.command.system.*;

import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initRegistry();
        initData();
        runArgs(args);
        logService.debug("Test environment.");
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    private void initData() {
        /*projectService.add(new Project("Project 1", "1", new Date(2003,01,01)));
        projectService.changeStatusByName("Project 1", Status.COMPLETED);
        projectService.add(new Project("Project 2", "2", new Date(2002,01,01)));
        projectService.add(new Project("Project 3", "3", new Date(2001,01,01)));
        projectService.changeStatusByName("Project 3", Status.IN_PROGRESS);
        taskService.add(new Task("Task a", "1", new Date(03,01,01)));
        taskService.changeStatusByName("Task a", Status.COMPLETED);
        taskService.add(new Task("Task b", "2", new Date(02,01,01)));
        taskService.add(new Task("Task c", "3", new Date(01,01,01)));
        taskService.changeStatusByName("Task c", Status.IN_PROGRESS);*/
    }

    private void initRegistry() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.borisyuk.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.borisyuk.tm.command.AbstractCommand.class)
                .stream()
                .sorted(Comparator.comparing(Class::getName))
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }

    }

    private boolean runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException();
        final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
        return true;
    }

    private void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() { return authService; }

    @NotNull
    @Override
    public IUserService getUserService() { return userService; }

}

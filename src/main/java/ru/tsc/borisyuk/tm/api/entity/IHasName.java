package ru.tsc.borisyuk.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IHasName {

    String getName();

    @Nullable void setName(@NotNull String name);

}

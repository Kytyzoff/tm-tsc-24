package ru.tsc.borisyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.model.Command;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @Nullable
    Collection<String> getListCommandName();

    @Nullable
    Collection<String> getListCommandArg();

    void add(AbstractCommand command);

}
